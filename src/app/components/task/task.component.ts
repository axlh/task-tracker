import { Component, OnInit } from '@angular/core';
import { TaskService } from '../../services/task.service';
import { Task } from '../../Task';
import { TaskItemComponent } from '../task-item/task-item.component';
import { AddTaskComponent } from '../add-task/add-task.component';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-task',
  standalone: true,
  imports: [ CommonModule, TaskItemComponent, AddTaskComponent ],
  templateUrl: './task.component.html',
  styleUrl: './task.component.css'
})
export class TaskComponent implements OnInit {

  tasks: Task[] = [];

  constructor(
    private taskService: TaskService
  ){

  }

  ngOnInit(): void {
      this.taskService.getTasks().subscribe((task) => {
        this.tasks = task;
      });
  }

  deleteTask(task: Task){
   this.taskService.deleteTask(task).subscribe(() => {
    this.tasks = this.tasks.filter(t => t.id !== task.id );
   });
  }

  toggleReminder(task: Task){
    task.reminder = !task.reminder;
    this.taskService.updateTaskReminder(task).subscribe();
  }

  addTask(task: Task){
    this.taskService.addTask(task).subscribe((task) => {
      this.tasks.push(task);
    });
  }

}
